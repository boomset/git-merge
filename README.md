# README #

Make merge operations easy then ever. Just let jira and code talk to eachother


### Usage ###

* pip install git+https://bitbucket.org/boomset/git-merge.git

Following command will help;
```bash
gitmerge -h
```

If not;

|Command|Description|Required|Default Value|
|---|---|:---:|:---:|
|*-b BRANCH, --branch BRANCH*|The branch name wanted to merge to.|Yes||
|*-u USERNAME, --username USERNAME*|Jira username|Yes||
|*-p PASSWORD, --password PASSWORD*|Jira password|Yes||
|*-m*|Mergable tasks will be taken from jira|No|False|
|*-y*|Ayways YES means on merge process there will be no input required|No|False|
|*-x*|Excluded tasks list|No|False|
|*--noinput*|all permissions granted|No|False|
|*--env*|release branch is for (qa, stage)| Yes||
