from __future__ import absolute_import

import argparse
from datetime import datetime
from subprocess import Popen, PIPE

import re
from jira.client import JIRA

from git_merge.settings import VERSION


def run_syscall(cmd):
    """
    run_syscall; handle sys calls this function used as shortcut.
    ::cmd: String, shell command is expected.
    """
    p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    out, err = p.communicate()
    return out.rstrip(), err


def chunks(lst, n_size):
    for i in range(0, len(lst), n_size):
        yield lst[i:i + n_size]


def list_to_display(lst):
    return '{}{}'.format(
        len(lst) > 5 and '\n' or '',
        '\n'.join(map(lambda x: ', '.join(x), list(chunks(sorted(list(set(list(lst))), key=lambda x: x), 5)))),
    )


class Colors:
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'


class GitMerge:
    def __init__(self, **kwargs):
        self.proj_path = run_syscall('pwd')[0]
        self.no_input = kwargs.get('no_input')
        self.username = kwargs.get('username')
        self.password = kwargs.get('password')
        self.token = kwargs.get('token')
        if not self.password and not self.token:
            raise Exception('JIRA required token or password')
        self.mergeable = kwargs.get('mergeable')
        self.env = kwargs.get('env')
        self.branch = GitMerge.get_branch(self.env, kwargs.get('branch'))
        self.always_yes = self.no_input or kwargs.get('always_yes')
        self.excluded = (kwargs.get('excluded') or '').split(',')
        self.extra = (kwargs.get('extra') or '').split(',')
        self.jira = self.set_jira()
        self.branches = self.mergeable and self.get_mergeable() or kwargs.get('branches', '').split(',')
        self.branches.extend(self.extra)
        self.branches = filter(lambda x: x, self.branches)
        self.get_permission()

    @staticmethod
    def get_branch(env='', branch_name=None):
        rebuild = True
        branch_name = branch_name or '{}{}'.format(env[0].upper(), datetime.now().strftime('%d%m%y'))
        run_syscall('git checkout master')
        out, err = run_syscall("git checkout {}".format(branch_name))
        if err.find('Switched to branch') != -1:
            user_choice = raw_input('\n{0}{1}{2}{3} {0}already exists do you want to rebuild?(Y/n): {3}'.format(
                Colors.BOLD, Colors.WARNING, branch_name, Colors.END
            ))
            while user_choice not in ('Y', 'n'):
                user_choice = raw_input('{0}Only Y/n please?: {1}'.format(Colors.BOLD, Colors.END))
            rebuild = user_choice == 'Y'
        if rebuild:
            run_syscall('git checkout master')
            run_syscall('git branch -D {}'.format(branch_name))
            run_syscall("git checkout master;git checkout -b {}".format(branch_name))

        return branch_name

    def set_jira(self):
        return JIRA(
            options={'server': 'https://boomset.atlassian.net'},
            basic_auth=(self.username, self.password or self.token)
        )

    def get_mergeable(self):
        result_set = []
        states = '(Done, Merged, "In Review", "Manual Testing", "Ready to Deploy")'
        if self.env == 'stage':
            states = ('(Done, "Manual Testing", "Ready to Deploy") or ("Developer test succeeded" = passed and '
                      'status in (Merged, Done, "Manual Testing", "Ready to Deploy", "Design Review"))')
        jql = ('(status in {}) and project in ("Web Platform") and sprint in openSprints() order by key'.format(states))
        block_size = 100
        block_num = 0
        while True:
            start_idx = block_num * block_size
            issues = map(lambda x: x.key, self.jira.search_issues(jql, start_idx, block_size))
            result_set.extend(issues)
            if not issues:
                break
            block_num += 1
        return sorted(list(set(result_set).difference(set(self.excluded))), key=lambda x: x)

    def check_conflicts(self, prev_branch):
        while True:
            out, _ = run_syscall('git status')
            if out.find('nothing to commit') != -1:
                if self.always_yes and prev_branch:
                    print('{}merged!{}'.format(Colors.GREEN, Colors.END))
                break
            if self.always_yes and prev_branch:
                print('{}failed!{}'.format(Colors.FAIL, Colors.END))
                prev_branch = None
            files = map(lambda x: x[1], re.findall('both (modified|added|deleted): *(?P<file>[\w\d/.]*)', out))
            print('{}{}{}'.format(Colors.FAIL, '\n'.join(files), Colors.END))
            raw_input('Please fix conflicts?(C)')
        return prev_branch

    def get_permission(self):
        print('\ngitmerge({})\n'.format(VERSION))
        print('\n{}merge operation will continues with the following tasks...\n{}'.format(
            Colors.WARNING, Colors.END))
        print('{}Release Branch: {}{}{}'.format(
            Colors.BOLD, Colors.GREEN, self.branch, Colors.END))
        print('{0}Branches Count on Release: {1}{3}{2}'.format(
            Colors.BOLD, Colors.GREEN, Colors.END, len(self.branches)
        ))
        print('{0}Branches on Release: {1}{3}{2}'.format(
            Colors.BOLD, Colors.GREEN, Colors.END, list_to_display(self.branches)
        ))

        if not self.no_input:
            user_choice = raw_input('\nContinue?(Y/n): ')
            while user_choice not in ('Y', 'n'):
                user_choice = raw_input('Only Y/n please. Continue? (Y/n): ')

            if user_choice == 'n':
                return False
        else:
            print('\n{}Merge operation continue without permission!...\n{}'.format(
                Colors.WARNING, Colors.END))

        prev_branch = None
        total_length = 0
        branches_count = len(self.branches)
        while True:
            if branches_count == 0:
                break
            branches_count = branches_count / 10
            total_length += 1
        for branch in self.branches:
            index = self.branches.index(branch)
            prev_branch = self.check_conflicts(prev_branch)
            user_choice = self.always_yes and 'Y' or raw_input('{}({}) {}{} is merging... Approve?(Y/n): '.format(
                Colors.GREEN, str(index + 1).zfill(total_length), branch, Colors.END))
            pass_branch = False
            while user_choice not in ('Y', 'n'):
                user_choice = raw_input('Only Y/n please. Continue? (Y/n): ')

            if user_choice == 'n':
                pass_branch = True

            if not pass_branch:
                if self.always_yes:
                    print '{}({}) {}{} is merging... '.format(Colors.WARNING, index + 1, branch, Colors.END),
                run_syscall('rm -fr "{}/.git/rebase-apply"'.format(self.proj_path))
                run_syscall('rm -fr "{}/.git/index.lock"'.format(self.proj_path))
                run_syscall('git checkout master')
                run_syscall('git branch -D {}'.format(branch))
                run_syscall('git fetch origin')
                _, err = run_syscall('git checkout origin/{0}'.format(branch))
                if err.find('did not match any file') == -1:
                    _, err = run_syscall('git checkout -b {0}'.format(branch))
                    _, err = run_syscall('git pull --rebase origin {}'.format(branch))
                    _, err = run_syscall('git checkout {}'.format(self.branch))
                    _, err = run_syscall("""git merge {0} -m 'Merged in '"{0}" --ff""".format(branch))
                prev_branch = branch

        prev_branch = self.check_conflicts(prev_branch)
        if prev_branch:
            print('{}merged!{}'.format(Colors.GREEN, Colors.END))
        else:
            print('')
        out, _ = run_syscall("""git log --oneline | grep "Merged in WP-[0-9]*\$" | cut -d" " -f 4""")
        print('')
        print('Merged: {0}{2}{1}'.format(
            Colors.GREEN, Colors.END,
            list_to_display(out.split('\n'))))
        print('')
        print('{}{}{} is ready for departure! '.format(Colors.GREEN, self.branch, Colors.END))
        return True


def main():
    parser = argparse.ArgumentParser(prog='gitmerge')
    parser.add_argument('-b', '--branch', dest='branch', help='The base branch name.')
    parser.add_argument('-u', '--username', dest='username', help='Jira username', required=True)
    parser.add_argument('-p', '--password', dest='password', help='Jira password')
    parser.add_argument('-m', dest='mergeable', help='collect all mergeable tasks', action='store_true')
    parser.add_argument('-y', dest='always_yes', action='store_true')
    parser.add_argument('-t', dest='branches', help='tasks lists to merge')
    parser.add_argument('-x', dest='excluded', help='excluded tasks')
    parser.add_argument('-e', dest='extra', help='extra tasks')
    parser.add_argument('--token', dest='token', help='Jira Token')
    parser.add_argument('--noinput', dest='no_input', help='Not to ask any permission', action='store_true')
    parser.add_argument('--env', dest='env', help='To locate environment', required=True, choices=['qa', 'stage'])
    args = parser.parse_args()
    GitMerge(**args.__dict__)
